# Structure and Interpretation of R programs

Structure of R programs refers to
how the source code text of the R program is structured.
This is about lexing and parsing, and
is a matter of deciding which of all the possibly input strings
are valid R programs.

Interpretation of R programs means what a program means.
There a few ways to talk about what a program means,
but the essence is:
what happens when the program runs?
What effect does it have?

It's _syntax_ that requires that parentheses bracket the
conditional expression in an `if`;
it's _semantics_ that dictates which of the two branches of the
`if` are evaluated.

## Structure

As a simplification let's first only consider
the case of a single file containing an R program.
This file is a sequence of bytes;
in ASCII each byte will correspond to a character,
such as DOLLAR,
but in UTF-8 it sometimes takes a short sequence of bytes to
represent some characters, such as A ACUTE.

So that's one structure.

Building up from that,
R converts certain sequences of characters into _tokens_:
A token in a programming language
is a bit like a word in a natural language,
though it also includes things that we would
normally think of as punctuation.

Some tokens:

    lm          # a name
    read.csv    # another name
    0.95        # a number (or numeric)
    "hot"       # a string
    + - * /     # various mathematical operators
    $ [[ [      # other operators
    = <- ->     # also operators
    %any%       # more or less anything can go between %

`?Syntax` gives a more complete list.
It is case sensitive and not very helpful.

There are also keyword tokens, such as `function` and `if`.
Textually these look like names,
but they have particular syntactic roles to play and
are recognised as a special token.
For example,
the token `function` introduces a function definition and
is a signal to the parser that
an argument list and a function body follow.
This is why you cannot create a variable called `function`.

Comments and whitespace are not tokens,
they are used to separate tokens from one another.
Sometimes a space is required to separate tokens,
sometimes it is not.

    a=-9

    a = - 9

    a<-7    # An assignment
    a < -7  # A comparison

    7->a    # Consider -> instead?

This part of processing an R program is called _lexical analysis_
(or lexing or tokenisation).
The output is a stream of tokens.
This is a notional stream in that
you don't get to see the stream of tokens,
but it exists internally.

### Parsing

The token stream is a linear flat thing.
It's one token after another.
_Parsing_ is the act of building the token stream into
a more structured format,
usually thought of and depicted as a tree,
called the _syntax tree_.

Consider an R expression, a fragment of an R program, like:

    c + t * f

Viewed as a stream of tokens, this is 5 tokens.
Viewed as a syntax tree, it looks something like this:

        +
       / \
      /   *
     /   / \
    c   t   f

In this tree, a node with children (an interior node) corresponds
to an operator, like + or *,
and a node without children (a leaf node) corresponds
to an atomic expression, like a name or a number.

R has a way of representing these trees in R itself.
If you try `as.list(quote(c + t * f))` you can see the result
of parsing the expression `c + t * f`.
It's a vector (a list is a generic vector)
that has `+` as its first element,
`c` as its second element (which is the left hand operand of `+`),
and `t * f` as its third element (which is the right hand operand of `+`).

### functions

Syntactically a function consists of the keyword `function`,
followed by the formal argument list which is
a parenthesised sequence of comma-separated expressions,
followed by the function body which is an expression.

It turns out that an a sequence of expressions in curly brackets
is still an expression.

### Quoting

Because some tokens are parsed as binary operators
with special syntactic rules,
it may be hard to treat them in other ways.

For example, you can't type

    help(>)

To get help on the greater-than comparison operator.

To help with this, you can _quote_ the name.

    help(`>`)

To quote a name, you put a backquote before and after the name.

### Rewriting

Note that it is possible to assign to the result of a function.

    `class(x) <- "foo"`

At least, syntactically that's what it looks like.
In actual fact,
R rewrites this to call the "class assign" function,
passing in the expressions `x` and `"foo"`.

It's the same as

    `` `class<-`(x, "foo") ``

Which is actual R code that you can type.

Something similar happens for _subassignment_.
If you've ever used R you've probably used subassignment.

It's `my.df[,2] = v`.
Assigning to a part of an object.
That is actually rewritten to, and is the same as,

    `` `[-<`(blah, v) ``




## Interpretation

### Environments

Assigning to a new symbol creates a new entry in our environment.

`ls()` can be used to show the symbols that are present in the environment.


### Delay

Consider the R command

    data(rivers)

Clearly this cannot work by evaluating the expression `rivers` and passing the value to the function `data`.



## Random notes

### man page cons cells:

    man R

    ...

    Set min number of fixed size obj's ("cons cells") to N

## Fun R facts

`<-` is a function, and you can call it so:

    `<-`(favourite, "strawberry")

So is `if`:

    `if`(x < 0, -x, x)

Perhaps even more surprisingly, even `{` is a function.
Spelled `` `{` ``.


## REFERENCES

[Silva2018]
https://rgaiacs.gitlab.io/sheffield-r-users-group-2018-07-03/
discussion of `reticulate` R package which embeds a Python
interpreter.

[R2018]
https://cran.r-project.org/doc/manuals/R-lang.html
"R Language Definition"
